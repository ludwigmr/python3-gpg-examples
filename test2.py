# SPDX-FileCopyrightText: 2020 Intevation GmbH
#
# SPDX-License-Identifier: BSD-3-Clause

"""Test to OpenPGP sign data for emails.
Dependencies:
 * pygpg 0.3
Authors:
 * Ludwig Reiter <ludwig.reiter@intevation.de>
 * Bernhard E. Reiter <bernhard@intevation.de>
"""

import gpg
import tempfile
import os
import unittest
import shutil


keydir = os.path.join(os.path.dirname(__file__), 'keys')


class SignTestCase(unittest.TestCase):

    import_keys = ['test1.sec', 'test1.pub', 'test2.gpg', 'test2.pub']
    gpg_conf_contents = ''

    def keyfile(self, key):
        return open(os.path.join(keydir, key), 'rb')

    def setUp(self):
        self._gpghome = tempfile.mkdtemp(prefix='tmp.gpghome')
        os.environ['GNUPGHOME'] = self._gpghome
        fp = open(os.path.join(self._gpghome, 'gpg.conf'), 'wb')
        fp.write(self.gpg_conf_contents.encode('UTF-8'))
        fp.close()

        # import requested keys into the keyring
        ctx = gpg.Context()
        for key in self.import_keys:
            with self.keyfile(key) as fp:
                ctx.key_import(fp)

    def tearDown(self):
        del os.environ['GNUPGHOME']
        shutil.rmtree(self._gpghome, ignore_errors=True)

    def test_sign_nomime(self):
        email_body = """Hello,

        this is my email body,
        which shall be signed."""

        ctx = gpg.Context()
        # key = ctx.get_key('5F50 3EFA C8C8 9323 D54C '
        #                   '2525 91B8 CD7E 1592 5678')
        # GnuPG v>=2.0.19 should allow fingerprints with spaces as user ids
        # but some versions (2.0.22 Ubuntu LTS and 2.0.30) have a defect here:
        # https://bugs.gnupg.org/gnupg/issue2382
        # Beside 2.1.11 works, it is better to fall back and do without spaces:
        key = ctx.get_key('5F503EFAC8C89323D54C252591B8CD7E15925678')
        ctx.signers = [key]

        plaintext = email_body.encode()
        signed, sigs = ctx.sign(plaintext, mode=gpg.constants.sig.mode.CLEAR)
        self.assertEqual(len(sigs.signatures), 1)
        sig = sigs.signatures[0]
        self.assertEqual(sig.type, gpg.constants.sig.mode.CLEAR)

        # let us verify the signature
        data, result = ctx.verify(signed)
        vsigs = result.signatures

        self.assertEqual(data.decode(), email_body + '\n')
        self.assertEqual(len(vsigs), 1)
        vsig = vsigs[0]
        self.assertEqual(vsig.fpr, '5F503EFAC8C89323D54C252591B8CD7E15925678')

    def test_encrypt(self):
        plaintext = b'Hello World\n'
        ctx = gpg.Context()
        ctx.armor = True
        recipient = ctx.get_key('2D22891B0EA69FBCA1808F2926F69225D182BFCE')
        ciphertext, _, _ = ctx.encrypt(plaintext, [recipient], sign=False,
                                       always_trust=True)

        # rewind ciphertext buffer, and try to decrypt:
        plaintext, _, _ = ctx.decrypt(ciphertext)
        self.assertEqual(plaintext, b'Hello World\n')


if __name__ == '__main__':
    unittest.main()
