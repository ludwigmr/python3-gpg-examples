This repository is used to provide examples for python3-gpg

Simple test, if you have everything installed:
$ python3 test.py
should pass two tests.

Test for mails:
$ python3 mail_test.py
Creates Email objects with deattached signature and encrypted and prints them
to stdout.

The interesting parts beyond that:
$ python3 signed_mail_w_attachments.py
Prints out a signed mail with a test attachment.

$ python3 encrypted_signed_mail.py
Prints out an encrypted and signed mail.

$ python3 encrypted_signed_mail_w_attachments.py
Prints out an encrypted and signed mail with an attachment.

Send a dummy encrypted mail with:
$ python3 send_auto_encrypted_mail.py <email-adress>
It tries to use the WKD to get the public key for the email-adress, and sends
an encrypted "lorem ipsum" mail to the adress.

All files use the mail_producer.py file.

Notice:
* the body of a mail must include a trailing newline ('\n')
* You can create an mbox file of a printed mail by including a
  From MAILER-DAEMON Fri Mai 18 10:00:34 2020
  to the top of a printed mail. Then you can import it into a mail client.
  The keys are in the keys/-directory, they are not-protected by passwords.
