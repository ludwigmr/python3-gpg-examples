"""Send automatic encrypted test mail to an email address.

Uses WKD to get the key.

Usage: python3 send_auto_encrypted_mail.py <email-address>

SPDX-FileCopyrightText: 2020 Intevation GmbH
SPDX-License-Identifier: BSD-3-Clause
"""
import gpg
import smtplib
import sys
from mail_producer import create_encrypted_and_signed_mail


def get_key_from_wkd_for(ctx, email):
    ctx.set_ctx_flag("auto-key-locate", "clear,nodefault,wkd")
    keys = ctx.keylist(email, mode=gpg.constants.keylist.mode.LOCATE)
    for key in keys:
        return key


def main():
    if len(sys.argv) != 2:
        print("Error, no or too many email addresses provided")
        print("Usage: python3 send_auto_encrypted_mail_w_wkd.py "
              "<email-address>")
        print("Send an encrypted test email to the <email>-address.")
        sys.exit(1)

    # check if at least version 1.12.0
    major = int(gpg.version.major)
    minor = int(gpg.version.minor)
    if major == 0 or (major == 1 and minor < 12):
        print("Error: need at least gpg 1.12.0")
        sys.exit(3)

    email_adress = sys.argv[1]
    ctx = gpg.Context()

    try:
        ctx.signers = [ctx.get_key('5F503EFAC8C89323D54C252591B8CD7E15925678')]
    except gpg.errors.KeyNotFound:
        print("Error: You must have the keys/test1... in your keyring")
        sys.exit(2)
    recipient_key = get_key_from_wkd_for(ctx, email_adress)
    if not recipient_key:
        print("Error: Couldn't find a public key for the email adress")
        sys.exit(2)

    test_body = """Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea
rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum
dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea
rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum
dolor sit amet.\n"""
    NO_ATTACHMENTS = None
    email = create_encrypted_and_signed_mail(
        "test1.intelmq@example.org",
        email_adress,
        recipient_key,
        "An example encrypted signed mail",
        test_body,
        NO_ATTACHMENTS, ctx)
    print(email)
    print("Sending mail")
    s = smtplib.SMTP('localhost')
    s.send_message(email)
    s.quit()
    print("Bye")


if __name__ == "__main__":
    main()
